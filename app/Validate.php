<?php

/**
    *Validate Class to validate build, update and query cube
*/

namespace App;
class Validate
{

    /**
     * Validate dimentions of the cube
     *
     * @param  int $n and int $m
     * @return bool
     */
    public function dimentions($n, $m)
    {
        $error = false;
        // validate n and m with constrains in readme.md
        if ($n < 0 || $n > 100 || $m < 0 || $m >1000){
            $error = true;
        }
        return $error;
    }

    /**
     * Validate function update in the cube
     *
     * @param  int $x, int $y, int $z, int $w, int $n 
     * @return bool
     */
    public function update($x, $y, $z, $w, $n)
    {
        $error = false;
        // validate x,y,z and w with constrains in readme.md
        if ($x < 0 || $y < 0 || $z < 0 || $x >= $n || $y >= $n || $z >= $n || $w < -1000000000 || $w > 1000000000){
            $error = true;
        }
        return $error;
    }


    /**
     * Validate function query in cube
     *
     * @param  int $x1, int $y1, int $z1, int $x2, int $y2, int $z2  
     * @return bool
     */
    public function query($x1, $y1, $z1, $x2, $y2, $z2, $n)
    {
        $error = false;
        // validate x1,y1,z1,x2,y2 and z2 with constrains in readme.md
        if ($x1 < 0 || $y1 < 0 || $z1 < 0 ||  $x1 > $x2 || $y1 > $y2 || $z1 > $z2|| $x2 > $n || $y2 > $n || $z2 > $n){
            $error = true;
        }
        return $error;
    }
}