<?php

/**
    *Cube Class to build matrix, calculate query and update
*/

namespace App;
class Cube
{
    // private attrs of the cube and construct action
    private $cube, $n, $m, $changed;
    function __construct($n, $m)
    {
        $this->m = $m;
        $this->n = $n;
        $this->changed = array();
        $this->buildCube($n);
    }

    private function buildCube($n)
    {
        $array = array_fill(0, $n+1, 0);

        $aux = array_fill(0, $n+1, $array);

        $cube = array_fill(0, $n+1, $aux);
        
        $this->cube = $cube;
    }

    public function getCube()
    {
        // Get method to attr cube
        return $this->cube;
    }

    public function getN()
    {
        // Get method to attr n
        return $this->n;
    }

    public function getM()
    {
        // Get method to attr m
        return $this->m;
    }

    public function getChanged()
    {
        // Get method to attr changed
        return $this->changed;
    }

    public function updateCube($x, $y, $z, $w)
    {
        // Set value of the cube in coords x,y,z
        $this->cube[$x][$y][$z] = $w;
        // Update array changed to store the coords that has been modified
        array_push($this->changed, [$x,$y,$z]);
    }

    public function queryCube($x1, $y1, $z1, $x2, $y2, $z2)
    {
        // Get sum of the cube with coords received
        $result = array();
        $tam_up = count($this->changed) -1;
        $sum = 0;
        // Loop in changed array
        for ($i=0; $i <= $tam_up; $i++){
            $x = $this->changed[$i][0];
            $y = $this->changed[$i][1];
            $z = $this->changed[$i][2];
            // verified if coords changed are in the coords received
            if ($x >= ($x1-1) && $x < $x2 && $y >= ($y1-1) && $y < $y2 && $z >= ($z1-1) && $z < $z2){
                // Add in array result the coords modified to sum the values
                array_push($result, [$x, $y, $z]);
                $sum += $this->cube[$x][$y][$z];
            }
        }
        return $sum;
    }
}