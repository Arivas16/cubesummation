<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    /**
     * Download final file with the result of test case
     *
     * @return \Illuminate\Http\Response
     */
    public function download()
    {
        // validate if the file exists to download or show error message
        if (file_exists('output.txt')){
            return response()->download('output.txt');
        }else{
            return redirect('/')->with('alert-danger', 'Ocurrió un error con el archivo de salida. Por favor, inténtelo nuevamente');
        }
    }
}
