<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cube;
use App\Validate;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\UploadFileFormRequest;

class CubeController extends Controller
{
    /**
     * Display the form to upload the file with cube test cases
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cubes.index');
    }

    /**
     * Read a file, build a new cube and calculate operations.
     * @param  App\Http\Requests\UploadFileFormRequest $request
     * @return \Illuminate\Http\Response
     */
    public function calculate(UploadFileFormRequest $request)
    {
        $file = $request->file('file');
        $file = $file->openFile();
        $content = $file->fread($file->getSize());
        $lines = explode("\n", $content);
        array_shift($lines);
        $res_final = "";
        // Open new file with the result of test case
        $resfile = fopen("output.txt", "w");
        $error = false;

        foreach($lines as $line) {
            // separate string by blank spaces
            $result = explode(" ", $line);
            if (!empty($result[0])){
                // validate if the line contains the n and m
                if (!str_contains($result[0], "UPDATE") && !str_contains($result[0], "QUERY")) {
                    // delete var cube
                    unset($cube);
                    unset($validate);
                    $n = intval($result[0]);
                    $m = intval($result[1]);
                    $validate = new Validate();
                    // validate dimentions with constrains
                    if ($validate->dimentions($n,$m)){
                        // break loop and report error
                        $error = true;
                        break;
                    }else{
                        // build a new cube with n and m
                        $cube = new Cube($n, $m);
                    }
                }else{
                    if ($result[0] == "UPDATE"){
                        // get the values to update the cube
                        $x = intval($result[1]) - 1;
                        $y = intval($result[2]) - 1;
                        $z = intval($result[3]) - 1;
                        $w = intval($result[4]);
                        $n = $cube->getN();
                        // validate coords in cube update with constrains
                        if($validate->update($x, $y, $z, $w, $n)){
                            // break loop and report error
                            $error = true;
                            break;
                        }else{
                            // update the cube with value w
                            $cube->updateCube($x, $y, $z, $w);
                        }
                    }else{
                        if ($result[0] == "QUERY"){
                            // get sum of the cube with coords x1, y1, z1, x2, y2 and z2
                            $x1 = intval($result[1]);
                            $y1 = intval($result[2]);
                            $z1 = intval($result[3]); 
                            $x2 = intval($result[4]);
                            $y2 = intval($result[5]);
                            $z2 = intval($result[6]);
                            $n = $cube->getN();
                            // validate coords in cube query with constrains
                            if ($validate->query($x1, $y1, $z1, $x2, $y2, $z2, $n)){
                                // break with loop and report error
                                $error = true;
                                break;
                            }else{
                                // get the sum with coords readed
                                $res = $cube->queryCube($x1, $y1, $z1, $x2, $y2, $z2);
                                $res_final = strval($res);
                                // write in final file the result of query
                                fwrite($resfile, $res_final);
                                fwrite($resfile, "\n");
                            }
                        }
                    }
                }
            }
        }
        fclose($resfile);

        // validate error or success to show correct message in view
        if (file_exists('output.txt') && !$error){
            $request->session()->flash('alert-success', 'El cubo fue procesado exitosamente');
        }else{
            $request->session()->flash('alert-danger', 'Ocurrió un error procesando el cubo. Por favor, inténtelo nuevamente');
        }
        return view('cubes.calculate');
    }

}
