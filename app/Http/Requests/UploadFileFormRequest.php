<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UploadFileFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required',
        ];
    }

    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            'file.required' => 'Debe cargar un archivo',
            'file.mimes'  => 'El archivo debe ser .txt',
        ];
    }
}
