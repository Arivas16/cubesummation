@extends('layouts.master')
 
@section('content')
<h5 class="text-center">Elige la opción de "Descargar" para obtener un archivo con los resultados. Para cargar un nuevo caso de prueba, selecciona la opción "Procesar otro caso de prueba"</h5>
<div class="btn-group btn-group-justified" role="group" aria-label="options">
@if(Session::has('alert-success'))
  <div class="btn-group" role="group">
  	{!! Html::linkAction('FileController@download', 'Descargar', array(), array('class' => 'btn btn-default')) !!}
  </div>
@endif
  <div class="btn-group" role="group">
  	{!! Html::linkAction('CubeController@index', 'Procesar otro caso de prueba', array(), array('class' => 'btn btn-default')) !!}
  </div>
</div>
@endsection