@extends('layouts.master')
 
@section('content')
<div class="container">
	<div class="row">
	  <div class="col-xs-12">
  		{!! Form::open(
			    array(
			        'route' => 'upload', 
			        'class' => 'form', 
			        'novalidate' => 'novalidate', 
			        'files' => true)) !!}

			@if (count($errors) > 0)
			<div class="alert alert-danger">
			    Ocurrió un error con la carga del archivo<br />
			    <ul>
			        @foreach ($errors->all() as $error)
			            <li>{{$error}}</li>
			        @endforeach
			    </ul>
			</div>
			@endif

			<div class="form-group">

			    {!! Form::label('Cargue el archivo') !!}
			    {!! Form::file('file', null) !!}
			    <p class="help-block">Por favor, cargue un archivo con extensión .txt</p>
			</div>

			<div class="form-group">
			    {!! Form::submit('Enviar', array('class' => 'btn btn-default')) !!}
			</div>
		{!! Form::close() !!}
	   </div>
	</div>

</div>
@endsection