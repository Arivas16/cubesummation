# README

cubesummation in php, to build 3-D Matrix in which each block contains 0 initially. The first block is defined by the coordinate (1,1,1) and the last block is defined by the coordinate (N,N,N). There are two types of queries:

UPDATE x y z W 
updates the value of block (x,y,z) to W

QUERY x1 y1 z1 x2 y2 z2
calculates the sum of the value of blocks whose x coordinate is between x1 and x2 (inclusive), y coordinate between y1 and y2 (inclusive) and z coordinate between z1 and z2 (inclusive)

Input Format
The first line contains an integer T, the number of test-cases. T testcases follow.
For each test case, the first line will contain two integers N and M separated by a single space.
N defines the N * N * N matrix.
M defines the number of operations.
The next M lines will contain either 
1. UPDATE x y z W
2. QUERY  x1 y1 z1 x2 y2 z2 

Output Format
Print the result for each QUERY.

Constrains
1 <= T <= 50
1 <= N <= 100
1 <= M <= 1000
1 <= x1 <= x2 <= N
1 <= y1 <= y2 <= N
1 <= z1 <= z2 <= N
1 <= x,y,z <= N
-109 <= W <= 109 

* PHP version

	ruby-5.5.9

* Laravel version

	5.1

* Configuration

	1. Install composer
		- Laravel utilizes Composer to manage its dependencies. First, download a copy of the composer.phar. Once you have the PHAR archive, you can either keep it in your local project directory or move to  usr/local/bin to use it globally on your system. On Windows, you can use the Composer Windows installer.

	2. Clone the project from repo
		- Run git clone https://Arivas16@bitbucket.org/Arivas16/cubesummation.git

	3. Dependencies
		- composer.json file has project dependencies 

	4. Deploy project
		- You need configure apache server to run the project

	5. Input
		- Open the URL configured in the step 4, in my case is http://laravel.dev/
		- Fill and send the form
